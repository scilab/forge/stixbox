<?xml version="1.0" encoding="UTF-8"?>

<!--
 *
 * This help file was generated from qqplot.sci using help_from_sci().
 *
 -->

<refentry version="5.0-subset Scilab" xml:id="qqplot" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:scilab="http://www.scilab.org"
          xmlns:db="http://docbook.org/ns/docbook">

  <refnamediv>
    <refname>qqplot</refname>
    <refpurpose>Create a QQ-plot</refpurpose>
  </refnamediv>


<refsynopsisdiv>
   <title>Calling Sequence</title>
   <synopsis>
   qqplot(x,y)
   qqplot(x,y,ps)
   
   </synopsis>
</refsynopsisdiv>

<refsection>
   <title>Parameters</title>
   <variablelist>
   <varlistentry><term>x :</term>
      <listitem><para> a n-by-1 matrix of doubles</para></listitem></varlistentry>
   <varlistentry><term>y :</term>
      <listitem><para> a n-by-1 matrix of doubles</para></listitem></varlistentry>
   <varlistentry><term>ps :</term>
      <listitem><para> a 1-by-1 matrix of strings, the plot symbol (default="*")</para></listitem></varlistentry>
   </variablelist>
</refsection>

<refsection>
   <title>Description</title>
   <para>
If two distributions are the same (or possibly linearly
transformed) the points should form an approximately straight
line.
   </para>
   <para>
A red solid line joining the first and third quartiles of x and y
is plot.
This is a linear fit of the order statistics of the two samples.
An extrapolation of this line below the first and over the third
quantile is performed, and plot as a red dotted line.
   </para>
   <para>
The qqplot function is partly compatible with Matlab.
   </para>
   <para>
</para>
</refsection>

<refsection>
   <title>Examples</title>
   <programlisting role="example"><![CDATA[
// Experiment A
x = distfun_poissrnd(10,100,1);
y = distfun_poissrnd(5,100,1);
scf();
qqplot(x,y);
// Change the symbol
scf();
qqplot(x,y,"r.");

// Experiment B
// Check a sample of uniform numbers:
// against normal numbers:
// uniform is obviously non-normal.
n=10000;
x=distfun_unifrnd(0,1,n,1);
p=linspace(0.01,0.99,100)';
q=quantile(x,p);
y=distfun_norminv(p,mean(x),sqrt(variance(x)));
scf();
qqplot(y,q,"bo");
xtitle("U(0,1) - n=10 000",..
"Normal Quantile","Uniform Quantile");

// Experiment C
// Check a sample of exponential numbers
// against normal numbers:
// exp is obviously non-normal.
n=10000;
x=distfun_exprnd(1,n,1);
y=distfun_normrnd(0,1,n,1);
scf();
qqplot(x,y,"bo");
xtitle("Exp(1) - n=10 000",..
"Exp Quantile","Normal Quantile");

// Experiment D
// Check two sample of normal numbers:
// it matches.
n=10000;
x1=distfun_normrnd(0,1,n,1);
x2=distfun_normrnd(0,2,n,1);
scf();
qqplot(x1,x2,"bo");
xtitle("n=10 000",..
"Normal(0,1)","Normal(0,2)");

// Experiment E
// Reference
// "Fitting data into probability distributions"
// Tasos Alexandridis
// Generate data that follow an
// exponential distribution with mu = 4
values = distfun_exprnd(4,100,1);
// Generate random Gaussian noise N(0,1)
noise = distfun_normrnd(0,1,100,1);
// Add noise to the exponential distributed data
// so as to look more realistic
realdata = values + abs(noise);
// Estimate the parameter
paramhat=mean(values)
syntheticData = distfun_exprnd(4.9918,100,1);
scf();
qqplot(realdata,syntheticData);

   ]]></programlisting>
</refsection>

<refsection>
   <title>Authors</title>
   <simplelist type="vert">
   <member>Copyright (C) 2013 - Michael Baudin</member>
   <member>Copyright (C) 2010 - DIGITEO - Michael Baudin</member>
   <member>Copyright (C) 1993 - 1995 - Anders Holtsberg</member>
   </simplelist>
</refsection>
</refentry>
