<?xml version="1.0" encoding="UTF-8"?>

<!--
 *
 * This help file was generated from stdjack.sci using help_from_sci().
 *
 -->

<refentry version="5.0-subset Scilab" xml:id="stdjack" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:scilab="http://www.scilab.org"
          xmlns:db="http://docbook.org/ns/docbook">

  <refnamediv>
    <refname>stdjack</refname>
    <refpurpose>Jackknife estimate of the standard deviation of a parameter estimate.</refpurpose>
  </refnamediv>


<refsynopsisdiv>
   <title>Calling Sequence</title>
   <synopsis>
   s=stdjack(x,T)
   [s,y]=stdjack(x,T)
   
   </synopsis>
</refsynopsisdiv>

<refsection>
   <title>Parameters</title>
   <variablelist>
   <varlistentry><term>x :</term>
      <listitem><para> a matrix of doubles</para></listitem></varlistentry>
   <varlistentry><term>T :</term>
      <listitem><para> a function or a list, the function which computes the empirical estimate from x.</para></listitem></varlistentry>
   <varlistentry><term>s :</term>
      <listitem><para> a 1-by-1 matrix of doubles, the estimate of the standard deviation</para></listitem></varlistentry>
   <varlistentry><term>y :</term>
      <listitem><para> a 1-by-n matrix of doubles, the values of T of the resamples, where n is the size of the parameter estimate</para></listitem></varlistentry>
   </variablelist>
</refsection>

<refsection>
   <title>Description</title>
   <para>
Jackknife estimate of the standard deviation of the parameter
estimate T(x).
   </para>
   <para>
The function T must have the following header:
<screen>
p=T(x)
</screen>
where <literal>x</literal> is the sample or the resample
and <literal>p</literal> is a m-by-1 matrix of doubles.
In the case where the parameter estimate has a more general
shape (e.g. 2-by-2), the shape of <literal>p</literal> is reshaped
into a column vector with <literal>m</literal> components.
   </para>
   <para>
See "T and extra arguments" for details on how to pass extra-arguments
to T.
   </para>
   <para>
The function is equal to
<screen>
sqrt(diag(covjack(x,T)))
</screen>
   </para>
   <para>
</para>
</refsection>

<refsection>
   <title>Examples</title>
   <programlisting role="example"><![CDATA[
n = 20;
x=distfun_chi2rnd(3,n,1);
m=mean(x) // Empirical mean
s=stdev(x)/sqrt(n) // Standard error for the mean
s=stdjack(x,mean) // Standard error with Jackknife
// Get y
[s,y]=stdjack(x,mean);
size(y)
// Estimate the standard deviation of the median
m=median(x)
s=stdjack(x,median)

// With extra arguments
x=distfun_chi2rnd(3,50,5);
mean(x,"r")
[s,y]=stdjack(x,list(mean,"r"));
size(y)

   ]]></programlisting>
</refsection>

<refsection>
   <title>Authors</title>
   <simplelist type="vert">
   <member>Copyright (C) 2013 - Michael Baudin</member>
   <member>Copyright (C) 2010 - DIGITEO - Michael Baudin</member>
   <member>Copyright (C) 1993 - 1995 - Anders Holtsberg</member>
   </simplelist>
</refsection>
</refentry>
