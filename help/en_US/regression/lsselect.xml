<?xml version="1.0" encoding="UTF-8"?>

<!--
 *
 * This help file was generated from lsselect.sci using help_from_sci().
 *
 -->

<refentry version="5.0-subset Scilab" xml:id="lsselect" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:scilab="http://www.scilab.org"
          xmlns:db="http://docbook.org/ns/docbook">

  <refnamediv>
    <refname>lsselect</refname>
    <refpurpose>Select a predictor subset for regression</refpurpose>
  </refnamediv>


<refsynopsisdiv>
   <title>Calling Sequence</title>
   <synopsis>
   [Q, I, B, BB,R2] = lsselect(y,x,crit,how,pmax,level)
   
   </synopsis>
</refsynopsisdiv>

<refsection>
   <title>Parameters</title>
   <variablelist>
   <varlistentry><term>y :</term>
      <listitem><para> a m-by-1 matrix of doubles, dependent variate (column vector)</para></listitem></varlistentry>
   <varlistentry><term>x :</term>
      <listitem><para> a m-by-n matrix of doubles, regressor variates</para></listitem></varlistentry>
   <varlistentry><term>crit :</term>
      <listitem><para> selection criterion (string): 'HT' : Hypothesis Test (default level = 0.05), 'AIC' : Akaike's Information Criterion, 'BIC': Bayesian Information Criterion, 'CMV' : Cross Model Validation (inner criterion RSS) (default = 'CMV')</para></listitem></varlistentry>
   <varlistentry><term>how :</term>
      <listitem><para> (string) choses between :'AS' : All Subsets, 'FI' : Forward Inclusion, 'BE' : Backward Elimination (default = 'FI')</para></listitem></varlistentry>
   <varlistentry><term>pmax :</term>
      <listitem><para> (scalar) limits the number of included parameters (default pmax=n).</para></listitem></varlistentry>
   <varlistentry><term>level :</term>
      <listitem><para> optional input argument, p-value reference used for inclusion or deletion.</para></listitem></varlistentry>
   <varlistentry><term>Q :</term>
      <listitem><para> criterion as a function of the number of parameters; might be interpreted as an estimate of the prediction standard deviation. For the method 'HT', Q is instead the successive p-values for inclusion or elimination.</para></listitem></varlistentry>
   <varlistentry><term>I :</term>
      <listitem><para> a 1-by-n matrix of doubles, index numbers of the included columns, i.e. the selected variables.</para></listitem></varlistentry>
   <varlistentry><term>B :</term>
      <listitem><para> a n-by-1 matrix of doubles, vector of coefficients, ie the suggested model is Y = X*B.</para></listitem></varlistentry>
   <varlistentry><term>BB :</term>
      <listitem><para> a n-by-pmax matrix of doubles, Column p of BB is the best B of parameter size p.</para></listitem></varlistentry>
   <varlistentry><term>R2 :</term>
      <listitem><para> a double, the R-squared statistics</para></listitem></varlistentry>
   </variablelist>
</refsection>

<refsection>
   <title>Description</title>
   <para>
Selects a good subset of regressors in a multiple linear
regression model.
The criterion is one of the following
ones, determined by the third argument, crit.
   </para>
   <para>
The fifth argument, pmax, limits the number of included
parameters. The returned Q is the criterion as a function of
the number of parameters; it might be interpreted as an
estimate of the prediction standard deviation. For the method
'HT' the reported Q is instead the successive p-values for
inclusion or elimination.
   </para>
   <para>
The last column of the prediction matrix x must be an intercept
column, ie all elements are ones. This column is never excluded
in the search for a good model. If it is not present it is added.
The output I contains the index numbers of the included columns.
For the method 'HT' the optional input argument level is the
p-value reference used for inclusion or deletion. Output B is
the vector of coefficients, ie the suggested model is
Y = X*B. Column p of BB is the best B of parameter size p.
   </para>
   <para>
This function is not highly optimized for speed but rather for
flexibility. It would be faster if 'all subsets' were in a
separate routine and 'forward' and 'backward' were in another
routine, especially for CMV.
   </para>
   <para>
</para>
</refsection>

<refsection>
   <title>Examples</title>
   <programlisting role="example"><![CDATA[
// Longley.dat contains 1 Response Variable y,
// 6 Predictor Variables x and 16 Observations.
// Source : [4]
[data,txt] = getdata(24);
Y=data(:,1);
X=data(:,2:7);
// Add a column of 1s at then end
nobs=size(X,"r");
X = [X,ones(nobs,1)];
[Q,I,B,BB,R2]=lsselect(Y,X);
// Draw parity plot
scf();
title(msprintf("Regression with selection - R2=%.2f%%",R2*100))
plot(Y,X*B,"bo")
plot(Y,Y,"r-")
xlabel("Observations")
ylabel("Predictions")

   ]]></programlisting>
</refsection>

<refsection>
   <title>See also</title>
   <simplelist type="inline">
   <member><link linkend="lsfit">lsfit</link></member>
   <member><link linkend="linreg">linreg</link></member>
   </simplelist>
</refsection>

<refsection>
   <title>Authors</title>
   <simplelist type="vert">
   <member>Copyright (C) 2016 - Michael Baudin</member>
   <member>Copyright (C) 2010 - DIGITEO - Michael Baudin</member>
   <member>Copyright (C) 1993 - 1995 - Anders Holtsberg</member>
   </simplelist>
</refsection>
</refentry>
