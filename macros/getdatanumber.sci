// Copyright (C) 2016 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function imax=getdatanumber()
    // Returns the number of datasets.
    //
    // Calling Sequence
    //   imax=getdatanumber
    //
    // Parameters
    //   imax : a 1-by-1 matrix of floating point integer, the number of datasets
    //
    // Description
    // Returns the number of datasets available in the getdata function.
    //
    // Examples
    // imax=getdatanumber()
    // 
    // Authors
    // Copyright (C) 2016 - Michael Baudin
    //

    [lhs, rhs] = argn()
    apifun_checkrhs ( "getdatanumber" , rhs , 0 )
    apifun_checklhs ( "getdatanumber" , lhs , 0:1 )
    //
    imax=24
endfunction
