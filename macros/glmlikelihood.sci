// Copyright (C) 2016 - 2017 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function ll=glmlikelihood(b,X,y)
    // Computes the likelihood of the GLM
    //
    // Calling Sequence
    // ll=glmlikelihood(b,X,y)
    //
    // Parameters
    // b : a (m+1)-par-1 matrix of doubles, the parameters of the model
    // X : a n-par-m matrix of doubles, the inputs of the model
    // y : a n-par-1 matrix of doubles, the data
    // ll : a n-par-1 matrix of doubles, the logarithm of the likelihood of the data
    //
    // Description
    // This function evaluates the logarithm of the likelihood 
    // of the data. 
    // A naive formula is :
    // <programlisting>
    // ll=log(yhat.^y.*(1-yhat).^(1-y))
    // </programlisting>
    // but the actual code uses a more robust formula.
    //
    // Examples
    // TODO
    //
    // Authors
    // Copyright (C) 2016 - 2017 - Michael Baudin

    yhat =glmval(b,X)
    mod=ieee()
    ieee(2)
    loglikelyhood=y.*log(yhat)+(1-y).*log(1-yhat)
    loglikelyhood(isnan(loglikelyhood))=0
    ieee(mod)
    ll=sum(loglikelyhood)
endfunction
