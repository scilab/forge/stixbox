// Copyright (C) 2016 - 2017 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function yhat=glmval(b,X)
    // Computes the value of the GLM
    //
    // Calling Sequence
    // yhat=glmval(b,X)
    //
    // Parameters
    // b : a (m+1)-par-1 matrix of doubles, the parameters of the model
    // X : a n-par-m matrix of doubles, the inputs of the model
    // yhat : a n-par-1 matrix of doubles, the prediction of the model
    //
    // Description
    // This function evaluates the value of the generalized linear model. 
    // It computes 
    // <programlisting>
    // c=b(1)+b(2)*X(:,1)+...+b(m+1)*X(:,m) 
    // </programlisting>
    // and then evaluates the Logistic cumulated density
    // <programlisting>
    // p=1/(1+exp(-c))
    // </programlisting>
    // By default, glmval adds a first column of 1s to X, 
    // corresponding to a constant term in the model. 
    // Do not enter a column of 1s directly into X. 
    //
    // Examples
    // TODO
    //
    // Authors
    // Copyright (C) 2016 - 2017 - Michael Baudin
    //
    // References
    // http://fr.mathworks.com/help/stats/glmfit.html
    // http://eric.univ-lyon2.fr/~ricco/cours/slides/regression_logistique.pdf

    dim=size(X,"c")
    apifun_checkveccol ( "glmval" , b ,   "b" ,  1, dim+1)
    //
    c=b(1)+X*b(2:$)
    yhat =1 ./ (1+exp(-c))
endfunction
