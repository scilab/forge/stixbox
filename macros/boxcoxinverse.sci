// Copyright (C) 2016 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function data = boxcoxinverse(transdat,lambda)
    // Inverse Box-Cox transformation
    //
    // Calling Sequence
    // data = boxcoxinverse(transdat,lambda)
    //
    // Parameters
    //   transdat : a m-by-1 matrix of doubles, the transformed data
    //   lambda : a m-by-1 matrix of doubles, the lambda parameter
    //   data : a m-by-1 matrix of doubles, greater or equal to zero, the data
    //
    // Description
    // boxcoxinverse applies the inverse Box-Cox transformation.
    //
    // The inverse Box-Cox transformation is defined by 
    //
    //<latex>
    //\begin{eqnarray}
    //x=\left(\lambda y + 1\right)^{1/\lambda}
    //\end{eqnarray}
    //</latex>
    //
    // if lambda is nonzero and 
    //
    //<latex>
    //\begin{eqnarray}
    //x=\exp(y)
    //\end{eqnarray}
    //</latex>
    //
    // if lambda=0.
    //
    // Uses a robust implementation which is accurate 
    // even when lambda is close to zero. 
    //
    // Transformed data points for which the inverse 
    // does not exist (i.e. when lambda*y+1<0 ) 
    // generate NAN floating point numbers.
    //
    // Examples
    // transdat=[0. 0.8921497 1.6538133 2.3414512 2.9788133]
    // lambda=0.7
    // data = boxcoxinverse(transdat,lambda)
    //
    // // Check accuracy when lambda is close to zero
    // data=boxcoxinverse(0.15,1.e-20)
    // exact=1.1618342427282831226
    //
    // // See when the inverse transformation does not exist
    // data = boxcoxinverse(-4:1,0.7)
    //
    // Authors
    // Copyright (C) 2016 - Michael Baudin

    [lhs, rhs] = argn()
    apifun_checkrhs ( "boxcoxinverse" , rhs , 2 )
    apifun_checklhs ( "boxcoxinverse" , lhs , 0:1 )
    //
    // Check Type
    apifun_checktype ( "boxcoxinverse" , transdat , "transdat" , 1 , "constant" )
    apifun_checktype ( "boxcoxinverse" , lambda , "lambda" , 2 , "constant" )
    //
    // Check Size
    apifun_checkvector ( "boxcoxinverse" , transdat , "transdat" , 1 )
    apifun_checkscalar ( "boxcoxinverse" , lambda , "lambda" , 1 )
    //
    // Check Content
    // Nothing to do
    //
    n=size(transdat,"*")
    if (lambda==0.) then
        data=exp(transdat)
    else
        data=zeros(transdat)
        // Put Nans where appropriate
        i=find(lambda*transdat+1>0)
        data(i)=exp(log1p(lambda*transdat(i))/lambda)
        i=find(lambda*transdat+1<=0)
        data(i)=%nan
    end
endfunction
