// Copyright (C) 2016 - Michael Baudin
// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (C) 1993 - 1995 - Anders Holtsberg
// Copyright (C) 2001-2002 - ENPC - Jean-Philippe Chancelier
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [z, p] = cmpmod(Y, Xsub, X);
    // Compare linear submodel versus larger one
    //
    // Calling Sequence
    // [z,p] = cmpmod(Y, Xsub, X)
    //
    // Parameters
    // Y : a m-by-1 matrix of doubles, the dependent variates vector
    // Xsub : a m-by-n1 matrix of doubles, controlled variates "X" matrix for the submodel
    // X : a m-by-n2 matrix of doubles, controlled variates "X" matrix for the model
    // z : a double, the F-test statistic value
    // p : a double, the p-value Prob(F variate > z)
    //
    // Description
    // Compare linear submodel versus larger one. 
    // The cmpmod function computes the F-test of a linear model 
    // against a smaller one. 
    // 
    // If the two models are very different, then the F-statistic is
    // large, leading to a p-value close to 0.
    // If the two models are similar, then the F-statistic is
    // small, leading to a p-value close to 1.
    //
    // Examples
    // // Longley.dat
    // [data,txt] = getdata(24);
    // Y=data(:,1);
    // X=data(:,2:7);
    // m=size(X,"r");
    // X=[ones(m,1),X];
    // // Create sub-model, close to the full model
    // Xsub=X(:,[1,2,3,4,5,7]);
    // [z,p] = cmpmod(Y, Xsub, X)
    // // Create sub-model, far away from the full model
    // Xsub=X(:,[1,3,5,7]);
    // [z,p] = cmpmod(Y, Xsub, X)
    //
    // See also 
    // lsfit
    //
    // Authors
    // Copyright (C) 2016 - Michael Baudin
    // Copyright (C) 2010 - DIGITEO - Michael Baudin
    // Copyright (C) 1993 - 1995 - Anders Holtsberg
    // Copyright (C) 2001-2002 - ENPC - Jean-Philippe Chancelier

    Y=Y(:)
    n = length(Y);
    ns = size(Xsub,2);
    nl = size(X,2);
    // Estimate sub-model
    Bsub = regress(Y,Xsub);
    Ysub = Xsub*Bsub;

    esub = Y-Ysub;
    // Estimate full model
    Bl = regress(Y,X);
    Yl = X*Bl;
    e = Y-Yl;
    // Compute sum of squares
    Rs = sum(esub.^2);
    Rl = sum(e.^2);
    // Compute F-statistic
    z = (Rs-Rl)/(nl-ns)/(Rl/(n-nl));
    // Compute P-Value
    if (nl==ns) then
        errmsg = msprintf(gettext("%s: The two models have the same number of parameters :%d."), "cmpmod", nl);
        error(errmsg)
    end
    v1 = nl-ns
    v2 = n-nl
    p = distfun_fcdf(z,v1,v2,%f)
endfunction
