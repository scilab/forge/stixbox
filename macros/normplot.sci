// Copyright (C) 2016 - Michael Baudin
// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (C) 1993 - 1995 - Anders Holtsberg
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function normplot(varargin)
    // Normal probability plot
    //
    // Calling Sequence
    // normplot(x)
    // normplot(x, symbol)
    //
    // Parameters
    // x : a n-by-m matrix of doubles, the data
    // symbol : a string, the symbol to plot (default="+")
    // 
    // Description
    // In this graphics, the data is plotted on x-axis and 
    // quantiles of the normal distribution are plotted on y-axis.
    // A straight red line is plotted based on the first and third 
    // quantiles of the data. 
    //
    // Interpretation
    // 
    // If the data is close to the normal distribution, 
    // then the points are close to a straight line. 
    // If the points are far away from the straight line, 
    // then the data is likely have a non-normal distribution. 
    //
    // Compatibility
    //
    // This function is partially compatible with Matlab's normplot. 
    // More precisely, the "h" output argument of Matlab is 
    // not available in Stixbox's normplot. 
    // This feature does not seem necessary, since the current handle 
    // can be get with "h=gcf()", as for any graphics. 
    // Furthermore, Matlab's normplot Y-axis is named 
    // "Probability", with unequal ticks corresponding to the 
    // normal quantiles : this Y-axis is associated with unequal 
    // spaces between the ticks, which makes the plot weird. 
    // Stixbox's normplot has the same Y-axis as presented in 
    // Wikipedia : this Y-axis is associated with equal 
    // spaces between the ticks. 
    //
    // Examples
    // x=distfun_normrnd(0,1,50,1);
    // scf();
    // normplot(x)
    // // Change the symbol
    // clf();
    // normplot(x,"b.")
    //
    // // See with uniform distribution
    // x=distfun_unifrnd(0,1,50,1);
    // clf();
    // normplot(x);
    //
    // // See with chi-square distribution
    // x=distfun_chi2rnd(2,50,1);
    // clf();
    // normplot(x);
    //
    // See Also
    // qqplot
    //
    // Authors
    // Copyright (C) 2016 - Michael Baudin
    // Copyright (C) 2010 - DIGITEO - Michael Baudin
    // Copyright (C) 1993 - 1995 - Anders Holtsberg
    //
    // Bibliography
    // https://en.wikipedia.org/wiki/Normal_probability_plot
    // http://fr.mathworks.com/help/stats/normplot.html

    // Check number of input arguments
    [lhs,rhs] = argn();
    apifun_checkrhs("normplot",rhs,1:2);
    apifun_checklhs("normplot",lhs,0:1);
    //
    x = varargin(1)
    symbol = apifun_argindefault ( varargin , 2 , "+" )
    //
    // Check type
    apifun_checktype("normplot",x,"x",1,"constant");
    apifun_checktype("normplot",symbol,"symbol",2,"string");
    //
    // Check size
    sz = size(x);
    n = sz(1);
    if n < 2 then
        str = msprintf(gettext("%s: Argument #%d: Matrix with at least %d rows expected.\n"), "normplot", 1, 2);
        error(str);
    end
    if size(symbol, '*') == 1 then
        symbol = symbol(ones(sz(1), sz(2)));
    elseif ~or(n == size(symbol)) then
        str = msprintf(gettext("%s: Argument #%d: Vector with %d elements expected.\n"), "normplot", 2, n);
        error(str);
    end
    
    MarksTable = [ "+" "o" "*" "." "x" "square" "diamond" "^" "v" ">" "<" "pentagram"];
    MarksStyleVal=[1 9 10 0 2 11 5 6 7 12 13 14];
    
    //
    p = ((1:n)'-1/2)/n;
    y = distfun_norminv(p,0,1);
    //
    for i=1:sz(2)
        data = x(:,i);
        // Sort the data in increasing order
        data = gsort(data,"g","i");
        
        //
        plot2d(data, y);
        e1 = unglue(gce());
        e1.line_mode = 'off';
        e1.mark_mode = 'on';
        e1.mark_foreground = i+1;
        e1.mark_style = MarksStyleVal(find(symbol(i)==MarksTable, 1));
        
        //
        i1=ceil(0.25*n);
        i3=ceil(0.75*n);
        x1=data(i1);
        x3=data(i3);
        y1=y(i1);
        y3=y(i3);
        t=linspace(data(1),data($),100)';
        straightline=(t-x1)*y3/(x3-x1) + (t-x3)*y1/(x1-x3);
        plot2d(t, straightline, i+1);
        e2 = unglue(gce());
        
        //
        glue([e1 e2]);
    end
    //
    xlabel("Data");
    ylabel("Theorical Quantiles");
endfunction
