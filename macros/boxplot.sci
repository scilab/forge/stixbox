// Copyright (C) 2014 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function boxplot(varargin)
    // Draw a box plot
    //  
    // Calling Sequence
    // boxplot(X)
    // boxplot(X,G)
    // boxplot(...,"plotstyle",plotstyle)
    // boxplot(...,"whisker",whisker)
    // boxplot(...,"symbol",symbol)
    // boxplot(...,"orientation",orientation)
    //
    // Parameters
    // X : a m-by-1 matrix of doubles or a m-by-n matrix of doubles or a list with n items, the data. If there are n columns in X, draw n boxes. If X is a list with n items, draw n boxes.
    // G : a m-by-1 matrix of strings, the groups.
    //
    // Description
    // Create a boxplot for the data in X. 
    // If X is a matrix of doubles, each column in X has an 
    // associated boxplot. 
    // If X is a list of n items, each item in X has 
    // associated boxplot.
    // By default, each column is associated 
    // with the integer label 1, 2, ..., n.
    //
    // To describe the boxplot, we must introduce the following 
    // variables :
    //
    // <screen>
    // x25=quantile(x,0.25) // First quartile
    // x50=quantile(x,0.50) // Median
    // x75=quantile(x,0.75) // Last quartile
    // IQR=x75-x25          // Inter-quartile range
    // </screen>
    // 
    // Each boxplot has the following structure.
    // <itemizedlist>
    //   <listitem>
    //     The median x50 is associated with a red thick line 
    //     in "traditional" style. 
    //     In "compact" style, a blue circle is drawn.
    //   </listitem>
    //   <listitem>
    //     The rectangle is drawn from x25 to x75 in "traditional" style.
    //     In "compact" style, a thick blue line is drawn.
    //   </listitem>
    //   <listitem>
    //     The max whisker is x75+w*IQR, 
    //     where w is the whisker length (see below for details). 
    //     (not in "compact" style)
    //   </listitem>
    //   <listitem>
    //     The min whisker is x25-w*IQR. (not in "compact" style)
    //   </listitem>
    //   <listitem>
    //     The outliers are the samples lower than the min 
    //     whisker or greater than the max whisker. 
    //     These outliers are drawn with the "symbol" option.
    //   </listitem>
    // </itemizedlist>
    //
    // The following options are managed.
    // <itemizedlist>
    //   <listitem>
    //     "whisker" : a double, >0, the whisker length (default=1.5).
    //      By default, if the data is normally distributed, 
    //      w=1.5 implies that 99.7% of the data is within the whiskers. 
    //   </listitem>
    //   <listitem>
    //     "plotstyle" : a string (by default="traditional").
    //     If plotstyle="compact", the boxplot has a narrower width.
    //   </listitem>
    //   <listitem>
    //     "symbol" : a string (by default="r+").
    //     The symbol used for outliers. 
    //     See the specifications in LineSpec.
    //   </listitem>
    //   <listitem>
    //     "orientation" : a string (by default="vertical").
    //     The orientation of the boxes. 
    //     If set to "horizontal", horizontal boxes are drawn 
    //     (this is useful when a large number of boxes 
    //     are plotted).
    //   </listitem>
    // </itemizedlist>
    //
    // Implementation notes:
    //
    // This function was designed to mimic Matlab's boxplot, 
    // but not all features are implemented yet.
    //
    // Examples
    // // Source: 
    // // Data : http://lib.stat.cmu.edu/datasets/cars.data
    // 
    // // 1. Get data
    // path=stixbox_getpath();
    // separator=" ";
    // conversion="double";
    // regexpcomments="/#/";
    // data=csvRead(fullfile(path,"demos","cars.csv"), ..
    //     separator, [], conversion, [], regexpcomments);
    // // 2. Extract Miles Per Gallon (column 1), 
    // // for each country (column 8).
    // // USA=1, JAPAN=2, EUROPE=3
    // MPG_USA=thrownan(-data(data(:,8)==1,1));
    // MPG_JAPAN=thrownan(-data(data(:,8)==2,1));
    // MPG_EUROPE=thrownan(-data(data(:,8)==3,1));
    // //
    // // 3. Make the boxplot with a list
    // scf();
    // boxplot(list(MPG_USA,MPG_JAPAN,MPG_EUROPE));
    // xlabel("Countries");
    // ylabel("Miles per gallon");
    // title("USA=1, JAPAN=2, EUROPE=3")
    // //
    // // 4. Make the boxplot with a column vector, and groups
    // X=[MPG_USA;MPG_JAPAN;MPG_EUROPE];
    // G_USA=repmat("USA",size(MPG_USA));
    // G_JAPAN=repmat("JAPAN",size(MPG_JAPAN));
    // G_EUROPE=repmat("EUROPE",size(MPG_EUROPE));
    // G=[G_USA;G_JAPAN;G_EUROPE];
    // scf();
    // boxplot(X,G);
    // //
    // // Configure outlier markers
    // scf();
    // boxplot(X,G,"symbol","bo");
    // //
    // // Configure orientation
    // scf();
    // subplot(1,2,1)
    // boxplot(X,G,"orientation","vertical"); // default
    // subplot(1,2,2)
    // boxplot(X,G,"orientation","horizontal");
    // //
    // // Make a boxplot for random numbers
    // x=distfun_normrnd(0,1,1000,25);
    // scf();
    // boxplot(x);
    // title("Normal(0,1) random numbers");
    // xlabel("Sample Index");
    // //
    // // Compare various boxplot for random numbers
    // x=distfun_normrnd(0,1,1000,25);
    // scf();
    // // Vertical and Traditional
    // subplot(2,2,1)
    // boxplot(x);
    // title("Vertical and Traditional");
    // // Vertical and compact
    // subplot(2,2,2)
    // boxplot(x,"plotstyle","compact");
    // title("Vertical and compact");
    // // Horizontal and compact
    // subplot(2,2,3)
    // boxplot(x,"orientation","horizontal","plotstyle","compact");
    // title("Horizontal and compact");
    // // Configure "whisker" width
    // subplot(2,2,4)
    // boxplot(x,"whisker",2.);
    // title("Configure whiskers");
    // //
    // // Configure "whisker" width, and "compact" style
    // scf();
    // x=distfun_normrnd(0,1,1000,25);
    // boxplot(x,"whisker",2.,"plotstyle","compact");
    // title("Normal(0,1) random numbers");
    // xlabel("Sample Index");
    // //
    // // Compare list vs groups on the same window
    // scf();
    // subplot(1,2,1)
    // boxplot(list(MPG_USA,MPG_JAPAN,MPG_EUROPE));
    // xlabel("Countries");
    // ylabel("Miles per gallon");
    // title("USA=1, JAPAN=2, EUROPE=3")
    // subplot(1,2,2)
    // boxplot(X,G);
    // xlabel("Countries");
    // ylabel("Miles per gallon");
    // 
    // Authors
    // Copyright (C) 2014 - Michael Baudin
    //
    // Bibliography
    // http://en.wikipedia.org/wiki/Box_plot
    [lhs,rhs] = argn();
    apifun_checkrhs("boxplot",rhs,1:6);
    apifun_checklhs("boxplot",lhs,0:1);
    //
    X=varargin(1)
    apifun_checktype("boxplot",X,"X",1,["constant","list"]);
    //
    // Get n, get the labels
    if (modulo(rhs,2)==1) then
        // boxplot(X,[option pairs])
        startpairs=2 
        X_ONLY=%t
    else
        // boxplot(X,G,[option pairs])
        startpairs=3 
        X_ONLY=%f
    end    

    if (X_ONLY) then
        // boxplot(X)
        if (typeof(X)=="constant") then
            [m,n]=size(X)
        elseif(typeof(X)=="list") then
            n=size(X)
        end
        datalabels=string(1:n)
    else
        // boxplot(X,G)
        G=varargin(2)
        apifun_checktype("boxplot",G,"G",2,"string");
        datalabels=unique(G)
        n=size(datalabels,"*")
    end
    //
    // 1. Set the defaults
    default=struct(..
    "plotstyle","traditional", ..
    "whisker",1.5,..
    "symbol","r+",..
    "orientation","vertical")
    //
    // 2. Manage (key,value) pairs    
    options=apifun_keyvaluepairs (default,varargin(startpairs:$))
    //
    // 3. Get parameters
    plotstyle = options.plotstyle
    whisker = options.whisker
    symbol = options.symbol
    orientation = options.orientation
    //

    // Check options
    apifun_checktype("boxplot",plotstyle,"plotstyle",startpairs,"string");
    apifun_checktype("boxplot",whisker,"whisker",startpairs,"constant");
    apifun_checktype("boxplot",symbol,"symbol",startpairs,"string");
    apifun_checktype("boxplot",orientation,"orientation",startpairs,"string");
    //
    apifun_checkscalar("boxplot",plotstyle,"plotstyle",startpairs)
    apifun_checkscalar("boxplot",whisker,"whisker",startpairs)
    apifun_checkscalar("boxplot",symbol,"symbol",startpairs)
    apifun_checkscalar("boxplot",orientation,"orientation",startpairs)
    //
    apifun_checkoption("boxplot",plotstyle,"plotstyle",startpairs,["traditional","compact"])
    tiny=number_properties("tiny")
    apifun_checkgreq("boxplot",whisker,"whisker",startpairs,tiny)
    apifun_checkoption("boxplot",orientation,"orientation",startpairs,["vertical","horizontal"])
    //
    // Draw boxes
    xmin=%inf // Min. of all samples
    for i=1:n
        if (X_ONLY) then
            if (typeof(X)=="constant") then
                data=X(:,i)
            elseif(typeof(X)=="list") then
                data=X(i)
            end
        else
            data=X(G==datalabels(i))
        end
        minbound=boxplot_data(i,data,plotstyle,whisker,symbol,orientation)
        xmin=min(xmin,minbound)
    end
    //
    // Remove axis, plot integer labels
    if (orientation=="vertical") then
        drawaxis(x=1:n,y=xmin,dir="d",tics="v",val=datalabels,sub_int=0)
        a=gca()
        a.axes_visible=["off","on","off"]
        a.data_bounds(1,2)=xmin
        a.box="off"
    else
        drawaxis(x=xmin,y=1:n,dir="l",tics="v",val=datalabels,sub_int=0)
        a=gca()
        a.axes_visible=["on","off","off"]
        a.data_bounds(1,1)=xmin
        a.box="off"
    end
endfunction

function minbound=boxplot_data(i,x,plotstyle,whisker,symbol,orientation)
    // Plot the boxplot for the i-th data in x.
    // Returns the minimum of min(x) and minwhisk.

    // Computed quartiles
    q=quantile(x,[0.25,0.5,0.75])
    x25=q(1)
    x50=q(2)
    x75=q(3)
    IQR=x75-x25  // Inter-quartile range
    // Min and max whiskers
    minwhisk=x25-whisker*IQR
    maxwhisk=x75+whisker*IQR
    // Outliers
    outliers=x((x<minwhisk)|(x>maxwhisk))
    //
    // Plot the rectangle
    if (orientation=="vertical") then
        if (plotstyle=="traditional") then
            xrect(i-0.4,x75,0.8,x75-x25)
        else
            plot([i,i],[x25,x75],"b-")
            e=gce()
            e.children.thickness=4
        end
    else
        if (plotstyle=="traditional") then
            xrect(x25,i+0.4,x75-x25,0.8)
        else
            plot([x25,x75],[i,i],"b-")
            e=gce()
            e.children.thickness=4
        end
    end
    //
    // Plot the lines
    if (orientation=="vertical") then
        if (plotstyle=="traditional") then
            plot([i-0.1,i+0.1],[minwhisk,minwhisk],"k-") // Lower bar
        end
    else
        if (plotstyle=="traditional") then
            plot([minwhisk,minwhisk],[i-0.1,i+0.1],"k-") // Left bar
        end
    end
    //
    // Median
    if (orientation=="vertical") then
        if (plotstyle=="traditional") then
            plot([i-0.4,i+0.4],[x50,x50],"r-") // Median : a thick red line
            e=gce()
            e.children.thickness=2
        else
            plot(i,x50,"bo") // Median : a blue circle
        end
    else
        if (plotstyle=="traditional") then
            plot([x50,x50],[i-0.4,i+0.4],"r-") // Median : a thick red line
            e=gce()
            e.children.thickness=2
        else
            plot(x50,i,"bo") // Median : a blue circle
        end
    end
    if (orientation=="vertical") then
        if (plotstyle=="traditional") then
            plot([i-0.1,i+0.1],[maxwhisk,maxwhisk],"k-") // Upper bar
        end
    else
        if (plotstyle=="traditional") then
            plot([maxwhisk,maxwhisk],[i-0.1,i+0.1],"k-") // Right bar
        end
    end
    if (orientation=="vertical") then
        plot([i,i],[minwhisk,x25],"k-") // Vertical line
        plot([i,i],[x75,maxwhisk],"k-") // Vertical line
    else
        plot([minwhisk,x25],[i,i],"k-") // Vertical line
        plot([x75,maxwhisk],[i,i],"k-") // Vertical line
    end
    //
    // Draw outliers
    if outliers~=[]
        if (orientation=="vertical") then
            plot(i*ones(outliers),outliers,symbol)
        else
            plot(outliers,i*ones(outliers),symbol)
        end
    end
    //
    minbound=min(min(x),x25-2.*IQR)
endfunction

