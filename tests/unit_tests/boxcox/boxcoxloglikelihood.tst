// Copyright (C) 2016 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

data = [
0.15	0.09	0.18	0.10	0.05	0.12	0.08
0.05	0.08	0.10	0.07	0.02	0.01	0.10
0.10	0.10	0.02	0.10	0.01	0.40	0.10
0.05	0.03	0.05	0.15	0.10	0.15	0.09
0.08	0.18	0.10	0.20	0.11	0.30	0.02
0.20	0.20	0.30	0.30	0.40	0.30	0.05
];
data=data(:);

// Special case
lambda=0.7;
boxcoxll = boxcoxloglikelihood(data,lambda);
exact=103.0322310945;
assert_checkalmostequal(boxcoxll,exact,1.e-6);

// Special case
lambda=-2;
boxcoxll = boxcoxloglikelihood(data,lambda);
exact=7.114669160535;
assert_checkalmostequal(boxcoxll,exact,1.e-6);

// Special case
lambda=0.;
boxcoxll = boxcoxloglikelihood(data,lambda);
exact=104.8276239498;
assert_checkalmostequal(boxcoxll,exact,1.e-6);
