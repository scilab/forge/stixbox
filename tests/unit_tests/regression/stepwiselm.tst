// Copyright (C) 2017 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- CLI SHELL MODE -->
// <-- NO CHECK REF -->

// Reference
// Applied Regression Analysis: A Research Tool Second Edition
// John O. Rawlings Sastry G. Pantula David A. Dickey
// Springer
// Page 163

path = stixbox_getpath ();
csvfile = fullfile(path,"tests","unit_tests","regression","data_Linthurst-1979.csv");
separator = " ";
decimal = ".";
// BIO SAL pH K Na Zn
data = csvRead(csvfile, separator, decimal);

m = size(data,"r");
BIO = data(:,1);
SAL = data(:,2);
pH = data(:,3);
K  = data(:,4);
Na  = data(:,5);
Zn = data(:,6);
X = [ones(m,1) SAL pH K Na Zn];
y = BIO;
// With default parameters
mdl = stepwiselm(y,X);
expected_selection = [1,0,1,0,1,0];
assert_checkequal(mdl.selection,expected_selection);
columnindices = find(expected_selection==1);
assert_checkequal(mdl.Z,X(:,columnindices));

// With "backward" and "AIC"
mdl = stepwiselm(y,X,"backward","AIC");
expected_selection = [1,1,1,1,0,1];
assert_checkequal(mdl.selection,expected_selection);
columnindices = find(expected_selection==1);
assert_checkequal(mdl.Z,X(:,columnindices));

// With "both"
mdl = stepwiselm(y,X,"both");
expected_selection = [1,0,1,1,0,0];
assert_checkequal(mdl.selection,expected_selection);
columnindices = find(expected_selection==1);
assert_checkequal(mdl.Z,X(:,columnindices));

// 
// Search an order 2 model
modelspec = 2;
[X,multiindices]=stepwiselm_generate([SAL pH K Na Zn],modelspec);
mdl = stepwiselm(y,X,"forward","AIC");
expected_selection = [1,0,1,0,0,0,0,0,0,0,1,1,0,0,0,0,1,0,0,0,0];
assert_checkequal(mdl.selection,expected_selection);
columnindices = find(expected_selection==1);
assert_checkequal(mdl.Z,X(:,columnindices));
// Print the selection
inputlabels = ["SAL" "pH" "K" "Na" "Zn"];
// Apply the selection on the model
selectionindices = find(mdl.selection==1)';
multiindices = multiindices(selectionindices,:);
str = stepwiselm_print(multiindices,inputlabels);
disp(str)

// 
// Search an order 2 model
modelspec = 2;
[X,multiindices]=stepwiselm_generate([SAL pH K Na Zn],modelspec);
// Print outputs
mdl = stepwiselm(y,X,"both",[],[],%t);
expected_selection = [1 0 0 0 0 1 0 0 0 0 0 0 1 0 0 1 0 0 0 0 0];
assert_checkequal(mdl.selection,expected_selection);
columnindices = find(expected_selection==1);
assert_checkequal(mdl.Z,X(:,columnindices));
assert_checkequal(mdl.selectionHistory,[1,2,4,6,8,10,12,14,16,18,20,13,-14,-12,-10,-20,-2,-4,-8,-18]);
assert_checkequal(size(mdl.criteriaHistory),[1,20]);
// Print the selection
inputlabels = ["SAL" "pH" "K" "Na" "Zn"];
// Apply the selection on the model
selectionindices = find(mdl.selection==1)';
multiindices = multiindices(selectionindices,:);
str = stepwiselm_print(multiindices,inputlabels);
disp(str)
// Use this selection to make a prediction
[B,bint,r,rint,stats,fullstats] = regress(y,mdl.Z);
yPredicted = mdl.Z*B;
