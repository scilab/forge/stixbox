// Copyright (C) 2016 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- CLI SHELL MODE -->

// Longley.dat contains 1 Response Variable y,
// 6 Predictor Variables x and 16 Observations.
// Source : [4]
[data,txt] = getdata(24);
Y=data(:,1);
X=data(:,2:7);
m=size(X,"r");
X=[ones(m,1),X];
//
// Create sub-model, close to the full model
Xsub=X(:,[1,2,3,4,5,7]);
[z,p] = cmpmod(Y, Xsub, X)
assert_checkalmostequal(z,0.0510991,1.e-6);
assert_checkalmostequal(p,0.8262118,1.e-6);
//
// Create sub-model, far away from the full model
Xsub=X(:,[1,3,5,7]);
[z,p] = cmpmod(Y, Xsub, X);
assert_checkalmostequal(z,14.602604,1.e-6);
assert_checkalmostequal(p,8.357398e-04,1.e-6);
