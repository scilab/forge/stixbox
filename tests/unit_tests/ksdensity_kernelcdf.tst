// Copyright (C) 2016 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// Plot available kernels
scf();
//
subplot(2,2,1)
title("Normal")
u=linspace(-3,3,100);
K=ksdensity_kernelcdf(u,"normal");
plot(u,K,"r-")
//
subplot(2,2,2)
title("Epanechnikov")
u=linspace(-1.5,1.5,100);
K=ksdensity_kernelcdf(u,"epanechnikov");
plot(u,K,"r-")
//
subplot(2,2,3)
title("Biweight")
u=linspace(-1.5,1.5,100);
K=ksdensity_kernelcdf(u,"biweight");
plot(u,K,"r-")
//
subplot(2,2,4)
title("Triangle")
u=linspace(-1.5,1.5,101);
K=ksdensity_kernelcdf(u,"triangle");
plot(u,K,"r-")
//
// Plot available kernels
scf();
xlabel("u");
ylabel("K(u)");
u=linspace(-1.5,1.5,101);
K=ksdensity_kernelcdf(u,"normal");
plot(u,K,"m-")
K=ksdensity_kernelcdf(u,"epanechnikov");
plot(u,K,"b--")
K=ksdensity_kernelcdf(u,"biweight");
plot(u,K,"r-.")
K=ksdensity_kernelcdf(u,"triangle");
plot(u,K,"g-")
legend(["Normal","Epanechnikov","Biweight","Triangle"]);
//
// Test numerical derivative
x=0.5;
J=numderivative(list(ksdensity_kernelcdf,"normal"),x);
assert_checkalmostequal(J,3.52065327D-01,1.e-6);
J=numderivative(list(ksdensity_kernelcdf,"epanechnikov"),x);
assert_checkalmostequal(J,5.62500000D-01,1.e-6);
J=numderivative(list(ksdensity_kernelcdf,"biweight"),x);
assert_checkalmostequal(J,5.27343750D-01,1.e-6);
J=numderivative(list(ksdensity_kernelcdf,"triangle"),x);
assert_checkalmostequal(J,0.5,1.e-6);
