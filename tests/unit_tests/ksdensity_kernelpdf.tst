// Copyright (C) 2014 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// Plot available kernels
scf();
//
subplot(2,2,1)
title("Normal")
u=linspace(-3,3,100);
K=ksdensity_kernelpdf(u,"normal");
plot(u,K,"r-")
//
subplot(2,2,2)
title("Epanechnikov")
u=linspace(-1.5,1.5,100);
K=ksdensity_kernelpdf(u,"epanechnikov");
plot(u,K,"r-")
//
subplot(2,2,3)
title("Biweight")
u=linspace(-1.5,1.5,100);
K=ksdensity_kernelpdf(u,"biweight");
plot(u,K,"r-")
//
subplot(2,2,4)
title("Triangle")
u=linspace(-1.5,1.5,101);
K=ksdensity_kernelpdf(u,"triangle");
plot(u,K,"r-")
//
// Plot available kernels
scf();
xlabel("u");
ylabel("K(u)");
u=linspace(-1.5,1.5,101);
K=ksdensity_kernelpdf(u,"normal");
plot(u,K,"m-")
K=ksdensity_kernelpdf(u,"epanechnikov");
plot(u,K,"b--")
K=ksdensity_kernelpdf(u,"biweight");
plot(u,K,"r-.")
K=ksdensity_kernelpdf(u,"triangle");
plot(u,K,"g-")
legend(["Normal","Epanechnikov","Biweight","Triangle"]);
//
// Test integral K(u) du = 1
atol=1.e-3;
rtol=1.e-3;
I=intg(-5,5,list(ksdensity_kernelpdf,"normal"),atol,rtol);
assert_checkalmostequal(I,1,[],atol);
I=intg(-1.5,1.5,list(ksdensity_kernelpdf,"epanechnikov"),atol,rtol);
assert_checkalmostequal(I,1,[],atol);
I=intg(-1.5,1.5,list(ksdensity_kernelpdf,"biweight"),atol,rtol);
assert_checkalmostequal(I,1,[],atol);
I=intg(-1.5,1.5,list(ksdensity_kernelpdf,"triangle"),atol,rtol);
assert_checkalmostequal(I,1,[],atol);

// Check value
x=0.5;
computed=ksdensity_kernelpdf(x,"normal");
assert_checkalmostequal(computed,3.52065327D-01,1.e-6);
computed=ksdensity_kernelpdf(x,"epanechnikov");
assert_checkalmostequal(computed,5.62500000D-01,1.e-6);
computed=ksdensity_kernelpdf(x,"biweight");
assert_checkalmostequal(computed,5.27343750D-01,1.e-6);
computed=ksdensity_kernelpdf(x,"triangle");
assert_checkalmostequal(computed,0.5,1.e-6);
