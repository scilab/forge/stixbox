// Copyright (C) 2013 - 2014 - Michael Baudin
// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (C) 1993 - 1995 - Anders Holtsberg
// Copyright (C) 1998 - 2000 - Mathematique Universite de Paris-Sud - Jean Coursol
// Copyright (C) INRIA
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

//STIXDEMO Demonstrate various stixbox routines.

function demostixbox()
    mprintf("Consider a sample X with some distribution.\n")

    n = 30;
    //    Y=distfun_chi2rnd(3,n,1);
    //    U=distfun_unifrnd(0,1,n,1);
    //    X = Y+3*U;
    mprintf("X:\n")
    X=[
    5.7897916    3.3175169    3.4540644    3.0111596    4.685514   
    3.7432393    2.4729398    2.5226272    7.2886706    3.0872539  
    4.8027145    2.0064461    5.3553894    3.0294776    1.9851152  
    1.1564012    4.3601624    1.4578598    3.3863574    0.6905131  
    8.4370623    9.9646268    4.2980891    2.5105713    1.9736246  
    3.9591496    3.2629708    7.1301829    2.7756571    4.1917893  
    ];
    disp(matrix(X,6,5))
    X=X(:)

    mprintf("Let us estimate the confidence intervals for the\n")
    mprintf("quantile of X. There are at least 3 ways to define\n")
    mprintf("an empirical quantile estimator. Look at the plot to see them\n")
    mprintf("applied to X.")

    vdata=gsort(X,"g","i");
    xmin=floor(min(vdata));
    xmax=ceil(max(vdata));
    scf();
    subplot(2,2,1)
    stairs(vdata,(1:n)'/n)
    plot(vdata,((1:n)'-0.5)/n,"b-")
    plot(vdata,((1:n)')/(n+1),"r-")
    xlabel("x")
    ylabel("P(X<x)")
    title("Empirical Cumulated Distribution")

    mprintf("Let us use the method from quantile(x,p,1) and compute\n")
    mprintf("some quantiles for X along with its standard deviation. The\n")
    mprintf("bootstrap is used for computing a standard deviation estimate\n")
    mprintf("which we use for plotting a 90 percent confidence interval through\n")
    mprintf("a normal approximation.")

    p = (0.1:0.1:0.9)';
    qx = quantile(X,p,1);
    sqx = stdboot(X,list(quantile,p,1),200);
    subplot(2,2,2)
    plot(p,qx-1.64*sqx,"b*-")
    plot(p,qx,"r*-")
    plot(p,qx+1.64*sqx,"b*-")
    xlabel("p")
    ylabel("Quantile")
    legend(["Bounds","Quantile Estimate"]);
    title("90% Confidence Interval of a quantile with stdboot")

    // 
    mprintf("Test bootstrap confidence intervals based on 1000 simulations.\n")

    Imb = ciboot(X,list(quantile,p),[],0.9,2000);
    subplot(2,2,4)
    plot(p,Imb(:,1),"b*-"); // Lower bound
    plot(p,Imb(:,2),"r*-"); // Estimate
    plot(p,Imb(:,3),"b*-"); // Upper bound
    xlabel("p")
    ylabel("Quantile")
    legend(["Bounds","Estimate"]);
    title("90% Confidence Interval of a quantile with ciboot")

    mprintf("A bootstrap confidence interval is based on the bootstrap\n")
    mprintf("distribution for some quantity. One might have a look at the\n")
    mprintf("distribution for it. The confidence interval for the standard\n")
    mprintf("deviation of X will serve as an example.\n")

    p=0.9
    [Imb,y] = ciboot(X,list(quantile,p),[],0.9,1000);
    subplot(2,2,3)
    histo(y,[],"Normalization","pdf");
    [f,xi]=ksdensity(y);
    plot(xi,f)
    xlabel("x")
    ylabel("Density")
    stitle=msprintf("Quantile at p=%.2f - Resamples from bootstrap",p)
    title(stitle)
    legend(["Resamples","KS density"]);

    //
    // Load this script into the editor
    //
    filename = "confidenceinterval.sce";
    dname = get_absolute_file_path(filename);
    editor ( fullfile(dname,filename) );

endfunction
demostixbox();
clear demostixbox;
