// This help file was automatically generated using helpupdate
// PLEASE DO NOT EDIT
demopath = get_absolute_file_path("stixbox.dem.gateway.sce");
subdemolist = [
"Empirical CDF of Exp", "demo_stairs.sce"; ..
"Bootstrap and Jackknife", "confidenceinterval.sce"; ..
];
subdemolist(:,2) = demopath + subdemolist(:,2)
